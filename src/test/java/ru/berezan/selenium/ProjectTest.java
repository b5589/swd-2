package ru.berezan.selenium;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class ProjectTest {

    @Test
    public void testCreate() throws IOException {
        final String url = "http://lk.volnenko.school/account/";
        final HttpGet httpGet = new HttpGet(url);
        final int statusCode = HttpClients.createDefault()
                .execute(httpGet)
                .getStatusLine()
                .getStatusCode();
        Assert.assertEquals(200,statusCode);
    }
}
