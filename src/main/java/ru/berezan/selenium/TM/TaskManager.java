package ru.berezan.selenium.TM;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskManager {

    private static List<Task> TASKS = new ArrayList<>();

    private static void addTask(Task task) {
        TASKS.add(task);
    }

    private static List<Task> findAll(){
        return TASKS;
    }

    private static void removeAll(){
        TASKS.clear();
    }

    public static void main(String[] args) {
        System.out.println("** TASK MANAGER **");

        final Scanner scanner = new Scanner(System.in);
        while (true) {

            System.out.println("** ENTER COMMAND **");
            final String cmd = scanner.nextLine();

            if ("HELP".equals(cmd)){
                System.out.println("[HELP]");
                System.out.println("add");
                System.out.println("list");
                System.out.println("clear");
                System.out.println("help");
            }

            if ("add".equals(cmd)) {
                System.out.println("[CREATE NEW TASK]");
                final Task task = new Task();
                System.out.println("ENTER NAME:");
                task.setName(scanner.nextLine());
                System.out.println("ENTER DESCRIPTION");
                task.setDescription(scanner.nextLine());
                addTask(task);
            }

            if ("clear".equals(cmd)){
                System.out.println("[CLEAR]");
                removeAll();
            }

            if ("list".equals(cmd)) {
                System.out.println("[TASKS]");
                int index = 1;
                for (final Task task:findAll()){
                    System.out.println(index + ". "+ task);
                    index++;
                }
            }
        }
    }

}
